//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProyectoSMP.Models
{
    using System;
    
    public partial class ConsultarUsuarios_Result
    {
        public int IdUsuario { get; set; }
        public string Identificacion { get; set; }
        public string Tipo_Identificacion { get; set; }
        public string Nombre { get; set; }
        public string Correo { get; set; }
        public string TipoCarga { get; set; }
        public string Provincia { get; set; }
        public string Canton { get; set; }
        public string Distrito { get; set; }
        public string Rol { get; set; }
        public bool Estado { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoSMP.Models.ViewModels
{
    public class ArchivoViewModel
    {
        public HttpPostedFileBase Archivo { get; set; }
    }
}